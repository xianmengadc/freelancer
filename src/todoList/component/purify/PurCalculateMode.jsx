import React from 'react';
import Input from '../common/Input.jsx';

import FinalCalculate from '../common/FinalCalculate.jsx';

function get(ref) {
    return +ref.refs[Object.keys(ref.refs)[0]].value;

}
// 如果算不出来，返回0
function getValidate(val) {
    if (isNaN(val) || val === Infinity) {
        return 0;
    }
    return (+val).toFixed(2);
}


// 表面粘度计算公式
class Viscosity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // Ф600 / 2
        let value = get(this.refs.a) / 2;
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'Ф600'} code={'a'} ref={'a'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 塑性粘度计算公式
class PlasticViscosity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // Ф600 / 2
        let value = get(this.refs.a) - get(this.refs.b);
        this.props.setValue(value, this.props.code, this.props.side);

        event.initEvent("triggerPlasticViscosity", true, true);
        event.PvValue = value;
        document.dispatchEvent(event);

        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'Ф600'} code={'a'} ref={'a'} />
            <Input name={'Ф300'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 动切力
class YieldPoint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // 0.479 * (2*Ф300-Ф600)
        let value = 0.479 * (2 * get(this.refs.b) - get(this.refs.a));
        this.props.setValue(value, this.props.code, this.props.side);

        event.initEvent("triggerYieldPoint", true, true);
        event.yieldPointValue = value;
        document.dispatchEvent(event);

        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'Ф600'} code={'a'} ref={'a'} />
            <Input name={'Ф300'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}
// 动塑比
class FreezePlastic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            initYieldPointValue: '',
            initPvValue: ''
        }
    }
    calculate() {
        //  动切力 / 动塑比
        let value = get(this.refs.a) / get(this.refs.b);
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">

            <Input name={'动切力(YP)'} code={'a'} ref={'a'} defaultValue={this.state.initYieldPointValue} />
            <Input name={'塑性粘度(VP)'} code={'b'} ref={'b'} defaultValue={this.state.initPvValue} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }

    componentDidMount() {
        this.eventListener1 = document.addEventListener('triggerYieldPoint', event => {
            this.setState({
                initYieldPointValue: event.yieldPointValue
            })
        }, false);
        this.eventListener2 = document.addEventListener('triggerPlasticViscosity', event => {
            this.setState({
                initPvValue: event.PvValue
            })
        }, false);
    }
    componentWillUnMount() {
        document.removeEventListener(this.eventListener1);
        document.removeEventListener(this.eventListener2);
    }
}

// 环空返速
class Loop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        //  1.2732 * Math.pow(10, 3) *泵排量 / (Math.pow(井眼直径, 2) - Math.pow(钻具外径, 2))
        let value = 1.2732 * Math.pow(10, 3) * get(this.refs.a) / (Math.pow(get(this.refs.b), 2) - Math.pow(get(this.refs.c), 2));
        this.props.setValue(value, this.props.code, this.props.side);

        event.initEvent("triggerLoop", true, true);
        event.loopValue = value;
        document.dispatchEvent(event);

        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'泵排量'} code={'a'} ref={'a'} />
            <Input name={'井眼直径'} code={'b'} ref={'b'} />
            <Input name={'钻具外径'} code={'c'} ref={'c'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 岩屑滑落速度
class SlipSpeed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        //  0.071*岩屑直径*(岩屑密度-钻井液密度)^0.667/(钻井液密度*有效视粘度)^0.333
        let value = 0.071 * get(this.refs.a) * Math.pow((get(this.refs.b) - get(this.refs.c)), 0.667) / Math.pow((get(this.refs.c) * get(this.refs.d)), 0.333);
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    onChange(code, value) {
        if(code === 'z') {
            this.setState({
                X_value:  value <= 1.08 ?  2 : 3
            })
        }
    }
    render() {
        return <div className="math-params">

            <Input name={'岩屑直径'} code={'a'} ref={'a'} />
            <Input name={'岩屑密度'} code={'b'} ref={'b'} />
            <Input name={'钻井液密度'} code={'c'} ref={'c'} onChange={this.onChange.bind(this)}/>
            <Input name={'有效视粘度'} code={'d'} ref={'d'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 岩屑净上升速度
class RisingSpeed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        //  环空返速-岩屑滑落速度
        let value = getValidate(get(this.refs.a)) - getValidate(get(this.refs.b));
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value,
            initLoopValue: ''
        });
    }
    render() {
        return <div className="math-params">

            <Input name={'环空返速'} code={'a'} ref={'a'} defaultValue={this.state.initLoopValue} />
            <Input name={'岩屑滑落速度'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }

    componentDidMount() {
        this.eventListener = document.addEventListener('triggerLoop', event => {
            this.setState({
                initLoopValue: event.loopValue
            })
        }, false);
    }
    componentWillUnMount() {
        document.removeEventListener(this.eventListener);
    }
}

// 流性指数
class FlowIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        //  3.322 * Math.log10(Ф600 / Ф300)
        let value = 3.322 * Math.log10(get(this.refs.a) / get(this.refs.b));
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'Ф600'} code={'a'} ref={'a'} />
            <Input name={'Ф300'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 稠度系数
class Coefficient extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        //  (0.511 * Ф300) / Math.pow(511, 3.322 * Math.log10(Ф600 / Ф300))
        let value = (0.511 * get(this.refs.b)) / Math.pow(511, 3.322 * Math.log10(get(this.refs.a) / get(this.refs.b)));
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">
            <Input name={'Ф600'} code={'a'} ref={'a'} />
            <Input name={'Ф300'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 井眼净化能力
class Ability extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            initLoopValue: ''
        }
    }
    calculate() {
        //  岩屑净上升速度/环空返速*100%
        let value = get(this.refs.a) / get(this.refs.b) * 100;
        this.props.setValue(value, this.props.code, this.props.side);
        this.setState({
            value: value
        });
    }
    render() {
        return <div className="math-params">

            <Input name={'岩屑净上升速度'} code={'a'} ref={'a'} />
            <Input name={'环空返速'} code={'b'} ref={'b'} defaultValue={this.state.initLoopValue} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>

        </div>
    }

    componentDidMount() {
        this.eventListener = document.addEventListener('triggerLoop', event => {
            this.setState({
                initLoopValue: event.loopValue
            })
        }, false);
    }
    componentWillUnMount() {
        document.removeEventListener(this.eventListener);
    }
}

class Total extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: [
                { label: '泵排量', value: '',unit:'（L/s）'},
                { label: '钻井液密度', value: '',specialControl: 'z', unit:'（g/cm3）' },
                { label: '井眼直径', value: '', unit:'（mm）' },
                { label: '钻具外径', value: '', unit:'（mm）' },
                { label: 'Ф600', value: '', unit:'（mPa.s）' },
                { label: 'Ф300', value: '', unit:'（mPa.s）' },
                { label: '岩屑直径', value: '',specialControl: 'x', unit:'（cm）' },
                { label: '有效视粘度', value: '', unit:'（mPa.s）' , ext:'hide'},
                { label: '岩屑密度', value: 2.5 , unit:'（g/cm3）', ext:'hide'}
            ],
            output: [

                { label: '流性指数', value: 0,unit:'(mm)' },
                { label: '稠度系数', value: 0 },
                { label: '表观粘度', value: 0, unit:'（mPa.s）' },
                { label: '塑性粘度', value: 0 , unit:'（mPa.s）'},
                { label: '动切力', value: 0, unit:'（Pa）' },
                { label: '动塑比', value: 0 },
                { label: '环空返速', value: 0, unit:'（m/s）' },
                { label: '有效视粘度', value: 0, unit:'（mPa.s）' },
                { label: '岩屑滑落速度', value: 0, unit:'（m/s）' },
                { label: '岩屑净上升速度', value: 0 , unit:'（m/s）'},
                { label: '井眼净化能力', value: 0, unit:'（%）' }
            ],
            X_value:''
        };
        this.formula = [
            '流性指数 = 3.322 * Math.log10(Ф600 / Ф300)',
            '稠度系数 = (0.511 * Ф300) / Math.pow(511, 3.322 * Math.log10(Ф600 / Ф300))',
            '表观粘度 = Ф600 / 2',
            '塑性粘度 = Ф600 -Ф300',
            '动切力 = 0.479 * (2*Ф300-Ф600)',
            '动塑比 = 动切力/塑性粘度',
            '环空返速 = 1.2732 * Math.pow(10, 3) *泵排量 / (Math.pow(井眼直径, 2) - Math.pow(钻具外径, 2))',
            '有效视粘度=塑性粘度+0.112*((井眼直径-钻具外径)*动切力/环空返速)',
            '岩屑滑落速度 = 0.071*岩屑直径*(岩屑密度-钻井液密度)^0.667/(钻井液密度*有效视粘度)^0.333',
            '岩屑净上升速度 = 环空返速-岩屑滑落速度',
            '井眼净化能力 = 岩屑净上升速度/环空返速*100%',
        ];
    }

    // 如果算不出来，返回0
    getValidate(val) {
        if (isNaN(val) || val === Infinity) {
            return 0;
        }
        return (+val).toFixed(2);
    }

    componentWillMount() {
        let inputs = Object.assign(this.state.input, []);
        let outputs = Object.assign(this.state.output, []);
        inputs.forEach(item => {
            if(item.label !== '岩屑密度') {
                item.value = window.localStorage.getItem(item.label);
            }
        });
        outputs.forEach(item => {
            item.value = window.localStorage.getItem(item.label);
        });
        // 更正岩屑直径
        // inputs[1].value <= 1.08 ? inputs[6].value = 2 : inputs[6].value = 3;
        // outputs.forEach(item => {
        //     item.value = window.localStorage.getItem(item.label);
        // });
        // if (this.state.input[1].value <= 1.08) {
        //     this.state.input[6].value = 2
        // } else {
        //     this.state.input[6].value = 3
        // }
        this.setState({
            input: inputs,
            output: outputs,
            X_value: +window.localStorage.getItem('岩屑直径') || 0,

        })
    }

    getValue(label) {
        let unit = this.totalParams.filter(item => {
            return item.label === label;
        });
        return Number(this.getValidate(unit[0].value))
    }

    setValue(inputParams) {
        let inputs = Object.assign(this.state.input, []);
        this.totalParams = inputParams;
        // v: 环空返速
        let v = 1.2732 * Math.pow(10, 3) * this.getValue('泵排量') / (Math.pow(this.getValue('井眼直径'), 2) - Math.pow(this.getValue('钻具外径'), 2));

        // v5: 动切力
        let v5 = 0.479 * (2 * this.getValue('Ф300') - this.getValue('Ф600'));

        // v4: 塑性粘度
        let v4 = this.getValue('Ф600') - this.getValue('Ф300');

        // v6: 有效视粘度
        let v6 = v4 + 0.112 * (this.getValue('井眼直径') - this.getValue('钻具外径')) * v5 / v;

        // v2: 岩屑滑落速度
        let tmp1 = Math.pow(((this.getValue('岩屑密度') - this.getValue('钻井液密度'))*1000), 0.667);
        // let tmp2 = Math.pow((this.getValue('钻井液密度') * this.getValue('有效视粘度')), 0.333);
        let tmp2 = Math.pow(((this.getValue('钻井液密度'))*1000 * this.getValidate(v6)), 0.333);
        let v2 = 0.071 * this.getValue('岩屑直径') * tmp1 / tmp2;

        // v3: 岩屑净上升速度
        let v3 = v - this.getValidate(v2);
        const outputs = Object.assign(this.state.output, []);

        // 流性指数
        outputs[0].value = this.getValidate((3.322 * Math.log10(this.getValue('Ф600') / this.getValue('Ф300'))));
        // 稠度系数
        outputs[1].value = this.getValidate((0.511 * this.getValue('Ф300')) / Math.pow(511, (3.322 * Math.log10(this.getValue('Ф600') / this.getValue('Ф300')))));
        // 表观粘度
        outputs[2].value = this.getValidate(this.getValue('Ф600') / 2);
        // 塑性粘度
        outputs[3].value = this.getValidate(v4);
        // 动切力
        outputs[4].value = this.getValidate(v5);
        // 动塑比
        outputs[5].value = this.getValidate(v5 / v4);
        // 环空返速
        outputs[6].value = this.getValidate(v);
        // 有效视粘度
        outputs[7].value = this.getValidate(v6);
        // 岩屑滑落速度
        outputs[8].value = this.getValidate(v2);
        // 岩屑净上升速度
        outputs[9].value = this.getValidate(v3);

        // 井眼净化能力
        outputs[10].value = this.getValidate(v3 / v * 100);

        outputs.forEach(item => {
            window.localStorage.setItem(item.label, item.value);
        });

        this.setState({
            output: outputs
        });
        this.props.setBack(outputs);

        // 给android 传参
        function GetQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
        const inputData = Object.assign(this.state.input, []);
        const outData = Object.assign(this.state.output, []);
        const url = GetQueryString('tab');
        const num=outData[outData.length-1].value;
        window.AndroidFunction.save(encodeURI(JSON.stringify(window.localStorage)), "净化能力", num, url);
    }

    setControl(specialControl, value) {
        if (specialControl === 'z') {
            this.setState({
                X_value:  value <= 1.08 ?  2 : 3
            })
        }
        else if (specialControl === 'x') {
            this.setState({
                X_value: value
            })
        }
    }


    render() {
        return <FinalCalculate inputParams={this.state.input}
        outputParams={this.state.output} setValue={this.setValue.bind(this)}
        title={'净化能力'} formula={this.formula}
        setControl={this.setControl.bind(this)}
        X_value={this.state.X_value} />
    }
}

module.exports = {
    Viscosity,
    PlasticViscosity,
    YieldPoint,
    FreezePlastic,
    Loop,
    SlipSpeed,
    RisingSpeed,
    FlowIndex,
    Coefficient,
    Ability,
    Total

};
