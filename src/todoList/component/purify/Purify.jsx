import React from 'react';
import Toolbar from './../common/ToolBar.jsx'
import Mode from './PurCalculateMode.jsx'
import './purify.less'
class Purify extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showConfigPanel: false,
            cur: 1,
            left: [
                {label: '表观粘度', code: 'surface-viscosity', value: 0},
                {label: '塑性粘度', code: 'plastic-viscosity', value: 0 },
                {label: '动切力', code: 'yield-point', value: 0 },
                {label: '动塑比', code: 'freeze-plastic', value: 0 },
                {label: '流性指数', code: 'flow-index', value: 0},
                {label: '稠度系数', code: 'consistency-coefficient', value: 0 },
            ],
            right: [
                {label: '岩屑滑落速度', code: 'slip-speed', value: 0 },
                {label: '岩屑净上升速度', code: 'rising-speed', value: 0},
                {label: '环空返速', code: 'loop', value: 0 },
                {label: '井眼净化能力', code: 'purify-ability', value: 0 }
            ],
            singleItem: ''
        };
    }

    componentWillMount() {
        let leftList = Object.assign(this.state.left, []);
        let rightList = Object.assign(this.state.right, []);
        leftList.forEach(item => {
            item.value = (+window.localStorage.getItem(item.label)).toFixed(2);
        })
        rightList.forEach(item => {
            item.value = (+window.localStorage.getItem(item.label)).toFixed(2);
        })


        this.setState({
            left: leftList,
            right: rightList
        })
    }

    setValue(v, code, side) {
        const tabs = Object.assign(this.state[side], []);
        let tab = tabs.filter(i => {return i.code === code})[0];
        tab.value = v;
        if(side === 'right') {
            this.setState({
                right: tabs
            })
        } else {
            this.setState({
                left: tabs
            })
        }
    }


    openSetting(item) {
        this.setState({
            showConfigPanel: true,
            singleItem: item
        })
    }

    leftConfig() {
        let list = [];
        this.state.left.forEach((item, key) => {
            list.push(<li key={key} className="left-param" onClick={this.openSetting.bind(this, item)}>
                    <span className="title"> {item.label} </span>
                    <span className="value"> {item.value} </span>
                </li>)
        });
        return list;
    }

    rightConfig() {
        let list = [];
        this.state.right.forEach((item, key) => {
            list.push(<li key={key} className="right-param" onClick={this.openSetting.bind(this, item)}>
                <span className="title"> {item.label} </span>
                <span className="value"> {item.value} </span>
            </li>)
        });
        return list;
    }

    showConfigPanel() {
        this.setState({
            showConfigPanel: true,
            singleItem: ''
        });
    }

    setFormula(formula) {
        if(formula === undefined) return null;
        if(typeof formula === 'string') {
            return <p> {formula} </p>
        } else {
            let list = [];
            formula.forEach((item, key) => {
                list.push(<p key={key}> {item} </p>)
            })
            return list;
        }
    }


    close() {
        this.setState({
            showConfigPanel: false
        })
    }

    setBack(values) {
        const left = Object.assign(this.state.left, []);
        const right = Object.assign(this.state.right, []);
        values.forEach(item => {
            left.forEach(leftUnit => {
                if(leftUnit.label === item.label) {
                    leftUnit.value = item.value;
                }
            });
            right.forEach(rightUnit => {
                if(rightUnit.label === item.label) {
                    rightUnit.value = item.value;
                }
            })
        });

        this.setState({
            left: left,
            right: right,
        })
    }

    configPanel() {
        if(this.state.showConfigPanel === true) {
            return (<div className='configPanel'>
                <div className='config' ref='configPanel'>
                    <img className='close' src='./image/icon/close.png' onClick={this.close.bind(this)}/>

                    <Mode.Total setBack={this.setBack.bind(this)}/>

                </div>
            </div>)
        }
        return null;
    }



    confirm() {
        this.setState({
            showConfigPanel: false
        })
    }

    render() {
        return <div className="purify-bg">
            {this.configPanel()}
            <div className="open-config" onClick={this.showConfigPanel.bind(this)}>设置</div>
            <div className="left-param-list">
                <ul>
                    {this.leftConfig()}
                </ul>
            </div>
            <div className="pump-bg">
                <img className="pure-img" src={`./image/purify/animate-${this.state.cur}.png`}/>
            </div>
            <div className="right-param-list">
                <ul>
                    {this.rightConfig()}
                </ul>
            </div>
            {/* <Toolbar navigate={this.props.navigate && this.props.navigate.bind(this)}/> */}
        </div>
    }
    componentDidMount() {
        let i = 2;
        this.timer =  setInterval(() => {
            if(i === 8) {
                i = 1;
            }
            this.setState({
                cur: i
            });
            i++
        }, 500)
    }

    componentWillUnmount() {
        clearTimeout(this.timer)
    }
}
export default Purify;
