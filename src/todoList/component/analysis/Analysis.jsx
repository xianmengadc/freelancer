import React from 'react';
import Tabs from './../common/Tabs.jsx';
import Toolbar from './../common/ToolBar.jsx'
import AnalysisConfigPanel from './AnalysisConfigPanel.jsx'
import AnalysisAnimation from './AnalysisAnimation.jsx'
// import AnalysisConfigPanel from '../effect/EffectCalculateMode.jsx'
import Mode from './CalculatMode.jsx';
import Mode1 from '../effect/EffectCalculateMode.jsx';
import Mode2 from '../recycle/RecycleCalculateMode.jsx';

import './analysis.less'
class Analysis extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            percent: 0,
            showConfigPanel: false,
            tabs: [
                {
                    label: '泵排量', imgUrl: 'ana-pump.png', code: 'pump', value: '',
                    formula: 'π*（缸套直径/2）^2*活塞冲程*缸套数*冲数*上水效率'
                },
                {
                    label: '总循环压耗', imgUrl: 'ana-cycle.png', code: 'cycle', value: '',
                    formula: [
                        '钻具环空压耗 = 7628 * 塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8* ((钻具长度/钻具内径)^4.82)',
                        '钻具循环压耗 = 7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82',
                        '公式中钻具指的就是钻杆、加重钻杆、钻铤、钻杆接箍、加重钻杆接箍'
                    ]
                },
                {
                    label: '喷嘴水功率', imgUrl: 'ana-drill.png', code: 'drill', value: '',
                    formula: '泵压 - 循环压耗'
                },
                {
                    label: '塑性粘度', imgUrl: 'ana-params.png', code: 'params', value: '',
                    formula: 'Ф600 - Ф300'
                },
                {
                    label: '环空返速', imgUrl: 'ana-loop.png', code: 'loop', value: '',
                    formula: '1.2732 * Math.pow(10, 3) *泵排量 / (Math.pow(井眼直径, 2) - Math.pow(钻具外径, 2))'
                },
                {
                    label: '流态', imgUrl: 'ana-flow.png', code: 'flow', value: '',
                    formula: ['有效视粘度 = 塑性粘度 + 0.112 * ((井眼直径 - 钻杆外径) * 动切力 / 环空返速)',
                        '环空雷诺数 = 928 * 环空液流的流速 * (井眼直径 - 钻杆外径) * 钻井液密度 * 1.0779 / 有效视粘度 * Math.pow(((2 * 环空流性指数 + 1) / (3 * 环空流性指数)), 环空流性指数)']
                }
            ],
            singleItem: ''
        };
    }

    componentWillMount() {
        let tabList = Object.assign(this.state.tabs, []);
        tabList.forEach(item => {
            if (typeof window.localStorage.getItem(item.label) === 'string') {
                item.value = window.localStorage.getItem(item.label)
            } else {
                item.value = (+window.localStorage.getItem(item.label)).toFixed(2);
            }
        })

        this.setState({
            tabs: tabList,

        })
    }

    setValue(v, code) {
        const tabs = Object.assign(this.state.tabs, []);
        let tab = tabs.filter(i => { return i.code === code })[0];
        tab.value = v;
        this.setState({
            tabs: tabs
        });
    }

    // 设置里面的每一项计算公式，是从以下 class 当中获取到的
    getCalculator(code, solo) {
        switch (code) {
            case 'pump':
                return <Mode.Pump code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            case 'cycle':
                // return <Mode2.Total code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            return <Mode.Cycle code={code}  isSolo={solo} setValue={this.setValue.bind(this)} />;
            case 'drill':
                return <Mode.Effect code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
                // return <Mode1.Total code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            case 'params':
                return <Mode.Params code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            case 'loop':
                return <Mode.Loop code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            case 'flow':
                return <Mode.Flow code={code} isSolo={solo} setValue={this.setValue.bind(this)} />;
            default:
                return <div> default </div>
        }
    }

    showConfigPanel() {
        this.setState({
            showConfigPanel: true,
            singleItem: ''
        });
    }

    // 弹出的设置界面是根据 this.state.tabs 里面内容进行渲染的
    // getTitle() {
    //     if (this.state.singleItem.code !== 'drill' && this.state.singleItem.code !== 'cycle') {
    //         return (<div className='config-title'>
    //             <span className='config-label'> {this.state.singleItem.label} </span>
    //         </div>)
    //     }
    // }
    getConfigs() {
        let list = [];
        if (this.state.singleItem !== '') {
            list.push(<li className='config-unit' key={this.state.singleItem.code}>
                {this.state.singleItem.code !== 'drill' && this.state.singleItem.code !== 'cycle'  && this.state.singleItem.code !== 'params' &&
                // {this.state.singleItem.code !== 'drill' &&
                <div className='config-title'>
                    <span className='config-label'> {this.state.singleItem.label} </span>
                </div>}
                <div className='config-control'>
                    {this.getCalculator(this.state.singleItem.code, 'solo')}
                </div>
            </li>)
        } else {
            this.state.tabs.forEach((item, key) => {
                list.push(<li className='config-unit' key={key}>
                    {/*{item.code !== 'drill' && item.code !== 'cycle' &&*/}
                    {item.code !== 'drill' && item.code !== 'params' && item.code !== 'cycle' &&
                    <div className='config-title'>
                        <span className='config-label'> {item.label} </span>
                    </div>}
                    {/* <div className='config-title'>
                        <span className='config-label'> {item.label} </span>
                    </div> */}
                    <div className='config-control'>
                        {this.getCalculator(item.code)}
                    </div>
                </li>)
            });
        }


        return list;
    }

    openSetting(item) {
        this.setState({
            showConfigPanel: true,
            singleItem: item
        })
    }


    confirm() {
        this.setState({
            showConfigPanel: false,
        })
    }

    cancel() {
        this.setState({
            showConfigPanel: false
        })
    }

    close() {
        this.setState({
            showConfigPanel: false
        })
    }

    // 点设置后，会弹出这个界面
    configPanel() {
        return <div className='configPanel' style={{ display: this.state.showConfigPanel === true ? '' : 'none' }}>
            <div className='config' ref='configPanel'>
                <img className='close' src='./image/icon/close.png' onClick={this.close.bind(this)} />
                {this.getConfigs()}
            </div>
        </div>

    }

    render() {

        return (<div className="ana-content">
            {this.configPanel()}
            <div className="shiubeng-bg">
                <div className="open-config" onClick={this.showConfigPanel.bind(this)}>设置</div>

                <AnalysisAnimation />
                <div className="water-bg" style={{ height: `${this.state.percent}%` }}>
                    <img className="water" src={`./image/icon/water.png`} />
                </div>
            </div>
            <AnalysisConfigPanel dataSource={this.state.tabs} openSetting={this.openSetting.bind(this)} />
            {/* <Toolbar navigate={this.props.navigate && this.props.navigate.bind(this)}/> */}
        </div>)
    }



}

export default Analysis;
