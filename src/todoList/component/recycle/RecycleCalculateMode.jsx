import React from 'react';
import Input from '../common/Input.jsx';
import FinalCalculate from '../common/FinalCalculate.jsx';

function get(ref) {
    return +ref.refs[Object.keys(ref.refs)[0]].value
}

function getRound(v) {
    if(v === Infinity){
        return 0;
    }else{
        return Math.round(v * 100) / 100;
    }

}

// 地面管汇压耗
class Pipe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // 地面管汇摩阻系数*钻井液密度*（泵排量/100）^1.86*9.818
        let value = get(this.refs.a) * get(this.refs.b) * Math.pow((get(this.refs.c) / 100), 1.86) * 9.818;
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }
    render() {
        return <div className="math-params">
            <Input name={'地面管汇摩阻系数'} code={'a'} ref={'a'} />
            <Input name={'钻井液密度'} code={'b'} ref={'b'} />
            <Input name={'泵排量'} code={'c'} ref={'c'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 钻杆内循环压耗
class InnerCycle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // 7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82
        let value = 7628 * Math.pow(get(this.refs.a), 0.2) * Math.pow(get(this.refs.b), 0.8) * Math.pow(get(this.refs.c), 1.8) * get(this.refs.d) / Math.pow(get(this.refs.e), 4.82);
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }
    render() {
        return <div className="math-params">

            <Input name={'塑性粘度'} code={'a'} ref={'a'} />
            <Input name={'钻井液密度'} code={'b'} ref={'b'} />
            <Input name={'泵排量'} code={'c'} ref={'c'} />
            <Input name={'钻具长度'} code={'d'} ref={'d'} />
            <Input name={'钻具内径'} code={'e'} ref={'e'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}
// 钻杆环空压耗
class BodyCycle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }

    calculate() {
        // 7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8
        let value = 7628 * Math.pow(get(this.refs.a), 0.2) * Math.pow(get(this.refs.b), 0.8) * Math.pow(get(this.refs.c), 1.8) * get(this.refs.d) / Math.pow((get(this.refs.e) - get(this.refs.f)), 3) / Math.pow((get(this.refs.e) + get(this.refs.f)), 1.8)
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }


    render() {
        return <div className="math-params">

            <Input name={'塑性粘度'} code={'a'} ref={'a'} />
            <Input name={'钻井液密度'} code={'b'} ref={'b'} />
            <Input name={'泵排量'} code={'c'} ref={'c'} />
            <Input name={'钻具长度'} code={'d'} ref={'d'} />
            <Input name={'井眼直径'} code={'e'} ref={'e'} />
            <Input name={'钻具外径'} code={'f'} ref={'f'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}





// 钻头压降
class Drill extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // v1 地面管汇压耗 = 地面管汇摩阻系数*钻井液密度*（泵排量/100）^1.86*9.818
        let value = get(this.refs.a) - get(this.refs.b);
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }
    render() {
        return <div className="math-params">
            <Input name={'泵压'} code={'a'} ref={'a'} />
            <Input name={'循环压耗'} code={'b'} ref={'b'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 其他钻具内循环
class OtherInner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // 非钻杆钻铤内循环压耗
        let value = get(this.refs.a);
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }
    render() {
        return <div className="math-params">
            <Input name={'非钻杆钻铤内循环压耗'} code={'a'} ref={'a'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}

// 其他钻具环空压耗
class OtherBody extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
    calculate() {
        // 非螺杆环空压耗+螺杆扶正器环空压耗
        let value = get(this.refs.a) + get(this.refs.b);
        this.props.setValue(value, this.props.code);
        this.setState({
            value: value
        })
    }
    render() {
        return <div className="math-params">
            <Input name={'螺杆环空压耗'} code={'a'} ref={'a'} />
            <Input name={'螺杆扶正器环空压耗'} code={'a'} ref={'a'} />
            <div>
                <span className='result'> 结果： {this.state.value} </span>
                <div className='cal-btn' onClick={this.calculate.bind(this)}>计算</div>
            </div>
        </div>
    }
}





class Total extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: [
                { label: '泵排量', value: '', unit:'（L/s）' },
                { label: '钻井液密度', value: '', unit:'（g/cm3）' },
                { label: '塑性粘度', value: '', unit:'（mPa.s）' },
                { label: '井眼直径', value: '', unit: '(mm)' },
                { label: '地面管汇摩阻系数', value: '' },
                { label: '钻杆内径', value: '', unit: '(mm)'  },
                { label: '钻杆外径', value: '' , unit: '(mm)' },
                { label: '钻杆长度', value: '', specialControl: 'm', unit: '(m)'  },
                { label: '加重钻杆内径', value: '', unit: '(mm)'  },
                { label: '加重钻杆外径', value: '', unit: '(mm)'  },
                { label: '加重钻杆长度', value: '', specialControl: 'n', unit: '(m)'  },
                { label: '钻铤内径', value: '', unit:'(mm)' },
                { label: '钻铤外径', value: '' , unit:'(mm)'},
                { label: '钻铤长度', value: '', unit: '(m)' },
                { label: '加重钻杆接箍外径', value: '', unit:'(mm)' },
                { label: '加重钻杆接箍长度', value: '', specialControl: 'q', unit: '(m)' },
                { label: '钻杆接箍外径', value: '', unit:'(mm)' },
                { label: '钻杆接箍长度', value: '', specialControl: 'p', unit: '(m)' },
                { label: '扶正器内径', value: '', unit:'(mm)' },
                { label: '扶正器外径', value: '', unit:'(mm)' },
                { label: '扶正器长度', value: '', unit: '(m)' },
                { label: '螺杆长度', value: '' , unit: '(m)'},
                { label: '螺杆外径', value: '', unit:'(mm)' },
                { label: '螺杆扶正器长度', value: '' , unit: '(m)'},
                { label: '螺杆扶正器外径', value: '' , unit:'(mm)'},
                { label: '非钻杆钻铤内循环压耗', value: '', unit:'（MPa）' },

                // { label: '泵压', value: '', ext: 'hide' }
                // { label: '泵压', value: '', ext:'hide' }
            ],
            output: [
                { label: '地面管汇压耗', value: 0, unit: '（MPa）' },
                { label: '钻杆内循环压耗', value: 0 , unit: '（MPa）'},
                { label: '钻杆环空压耗', value: 0 , unit: '（MPa）'},
                { label: '加重钻杆内循环压耗', value: 0 , unit: '（MPa）'},
                { label: '加重钻杆环空压耗', value: 0 , unit: '（MPa）'},
                { label: '钻铤内循环压耗', value: 0, unit: '（MPa）' },
                // { label: '钻头压降', value: 0, ext: 'hide' },
                { label: '钻铤环空压耗', value: 0 , unit: '（MPa）'},
                { label: '钻杆接箍环空压耗', value: 0 , unit: '（MPa）'},
                { label: '加重钻杆接箍环空压耗', value: 0 , unit: '（MPa）'},
                { label: '扶正器内循环压耗', value: 0, unit: '（MPa）' },
                { label: '扶正器环空压耗', value: 0, unit: '（MPa）' },
                { label: '螺杆环空压耗', value: 0, unit: '（MPa）' },
                { label: '螺杆扶正器环空压耗', value: 0 , unit: '（MPa）'},
                { label: '总循环压耗', value: 0 , unit: '（MPa）'},
                { label: '其他钻具环空压耗', value: '' , ext:'hide'},
                { label: '其他钻具内循环压耗', value: '' , ext:'hide'},
                { label: '钻杆环空压耗(含接箍)', value: '', ext:'hide'},
                { label: '加重钻杆环空压耗(含接箍)', value: '' , ext:'hide'},
                { label: '钻具内循环压耗', value: '' , ext:'hide'},
                { label: '钻具环空压耗', value: '', ext:'hide' }
            ],
            P_value: '',
            Q_value: ''
        };
        this.formula = [
            '地面管汇压耗 = 地面管汇摩阻系数*钻井液密度*（泵排量/100）^1.86*9.818',
            '钻具循环压耗 = 7628 * 塑性粘度^0.2*钻井液密度^0.8 * 泵排量^1.8 * 钻具长度/钻具内径^4.82',
            '钻具环空压耗 = 7628 * 塑性粘度^0.2*钻井液密度^0.8 * 泵排量^1.8 * 钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8',
            '钻头压降 = 泵压 - 循环压耗',
            '公式中钻具指的就是钻杆、加重钻杆、钻铤、钻杆接箍、加重钻杆接箍'
        ];

    }

    componentWillMount() {
        let inputs = Object.assign(this.state.input, []);
        let outputs = Object.assign(this.state.output, []);
        inputs.forEach(item => {
            item.value = window.localStorage.getItem(item.label);
        });
        outputs.forEach(item => {
            item.value = window.localStorage.getItem(item.label);
        });
        this.setState({
            input: inputs,
            output: outputs,
            P_value: +window.localStorage.getItem('钻杆接箍长度') || 0,
            Q_value: +window.localStorage.getItem('加重钻杆接箍长度') || 0
        });
    }


    getValue(label) {
        let unit = this.totalParams.filter(item => {
            return item.label === label;
        });
        return +unit[0].value
    }

    // 如果算不出来，返回0
    getValidate(val) {
        if (isNaN(val) || val === Infinity) {
            return 0;
        }
        return (+val).toFixed(2);
    }

    // 环空压耗统一计算公式, param1: 钻具长度, param2: 钻具外径
    getPressure(param1, param2) {
        return 7628 * Math.pow(this.getValue('塑性粘度'), 0.2) * Math.pow(this.getValue('钻井液密度'), 0.8) * Math.pow(this.getValue('泵排量'), 1.8) * param1 / Math.pow((this.getValue('井眼直径') - param2), 3) / Math.pow((this.getValue('井眼直径') + param2), 1.8)
    }

    // 内循环压耗统一计算公式, param1: 钻具长度, param2: 钻具内径
    getRecycle(param1, param2) {
        return 7628 * Math.pow(this.getValue('塑性粘度'), 0.2) * Math.pow(this.getValue('钻井液密度'), 0.8) * Math.pow(this.getValue('泵排量'), 1.8) * param1 / Math.pow(param2, 4.82);
    }

    setValue(inputParams) {
        this.totalParams = inputParams;


        const outputs = Object.assign(this.state.output, []);

        /*   // v1: 循环压耗
           let v1 = 7628 *  Math.pow(this.getValue('塑性粘度'),0.2) * Math.pow(this.getValue('钻井液密度'),0.8) * Math.pow(this.getValue('泵排量'),1.8) * this.getValue('钻杆加重钻杆钻铤长度')/Math.pow(this.getValue('钻杆加重钻杆钻铤内径'),4.82);

           // v2: 环空压耗
           let v2 = 7628 * Math.pow(this.getValue('塑性粘度'),0.2) * Math.pow(this.getValue('钻井液密度'),0.8) * Math.pow(this.getValue('泵排量'),1.8) * this.getValue('钻杆加重钻杆钻铤长度') / Math.pow((this.getValue('井眼直径')-this.getValue('钻杆加重钻杆钻铤外径')), 3) / Math.pow((this.getValue('井眼直径')+this.getValue('钻杆加重钻杆钻铤外径')), 1.8)
   */
        // v0: 地面管汇压耗
        let v0 = this.getValue('地面管汇摩阻系数') * this.getValue('钻井液密度') * Math.pow((this.getValue('泵排量') / 100), 1.86) * 9.818;



        // v1: 钻杆内循环压耗
        let v1 = this.getRecycle(this.getValue('钻杆长度'), this.getValue('钻杆内径'));

        // v2: 加重钻杆内循环压耗
        let v2 = this.getRecycle(this.getValue('加重钻杆长度'), this.getValue('加重钻杆内径'));

        // v3: 钻铤内循环压耗
        let v3 = this.getRecycle(this.getValue('钻铤长度'), this.getValue('钻铤内径'));

        // v5: 钻杆环空压耗
        let v5 = this.getPressure(this.getValue('钻杆长度'), this.getValue('钻杆外径'));
        // console.log(v5)

        // v6: 加重钻杆环空压耗
        let v6 = this.getPressure(this.getValue('加重钻杆长度'), this.getValue('加重钻杆外径'));

        // v7: 钻铤环空压耗
        let v7 = this.getPressure(this.getValue('钻铤长度'), this.getValue('钻铤外径'));

        // v8: 钻杆接箍环空压耗, lenP: 钻杆接箍长度，计算公式为 钻杆长度/9.5 * 0.5
        let lenP = this.state.P_value;
        let v8 = this.getPressure(lenP, this.getValue('钻杆接箍外径'));

        // window.localStorage.setItem('zgjg', v8);

        // v9: 加重钻杆接箍环空压耗,  lenQ: 加重钻杆接箍，计算公式为 加重钻杆长度/9.5 * 0.5
        let lenQ = this.state.Q_value;
        let v9 = this.getPressure(lenQ, this.getValue('加重钻杆接箍外径'));
        // window.localStorage.setItem('jzzgjg', v9);

        // v11: 扶正器内循环压耗
        let v10 = this.getRecycle(this.getValue('扶正器长度'), this.getValue('扶正器内径'));
        // v12: 扶正器环空压耗
        let v11 = this.getPressure(this.getValue('扶正器长度'), this.getValue('扶正器外径'));
        // v13: 螺杆环空压耗
        let v12 = this.getPressure(this.getValue('螺杆长度'), this.getValue('螺杆外径'));
        // v14: 螺杆扶正器环空压耗
        let v13 = this.getPressure(this.getValue('螺杆扶正器长度'), this.getValue('螺杆扶正器外径'));

        // v15: 其他钻具环空压耗
        let v15 = getRound(v12) + getRound(v13)
        // v16: 钻杆环空压耗(含接箍)
        let v16 = getRound(v5) + getRound(v8)

        // v17: 加重钻杆环空压耗(含接箍)
        let v17 = getRound(v6) + getRound(v9)

        // v18: 钻具内循环压耗
        let v18 = getRound(v1) + getRound(v2)+ getRound(v3)+ getRound(v10)+ getRound(this.getValue('非钻杆钻铤内循环压耗'))
        // v19: 钻具环空压耗
        let v19 = getRound(v6) + getRound(v5)+getRound(v7) + getRound(v8)+getRound(v11) + getRound(v9)+ getRound(v12)+getRound(v13)


        outputs[0].value = this.getValidate(v0); //地面管汇压耗
        outputs[1].value = this.getValidate(v1); // 钻杆内循环压耗
        outputs[2].value = this.getValidate(v5); // 钻杆环空压耗
        outputs[3].value = this.getValidate(v2); // 加重钻杆内循环压耗
        outputs[4].value = this.getValidate(v6); // 加重钻杆环空压耗
        outputs[5].value = this.getValidate(v3); // 钻铤内循环压耗

        outputs[6].value = this.getValidate(v7); // 钻铤环空压耗
        outputs[7].value = this.getValidate(v8); // 钻杆接箍环空压耗
        outputs[8].value = this.getValidate(v9); // 加重钻杆接箍环空压耗

        outputs[9].value = this.getValidate(v10); // 扶正器内循环压耗
        outputs[10].value = this.getValidate(v11); // 扶正器环空压耗
        outputs[11].value = this.getValidate(v12); // 螺杆环空压耗
        outputs[12].value = this.getValidate(v13); // 螺杆扶正器环空压耗

        outputs[14].value = this.getValidate(v15); // 其他钻具环空压耗
        outputs[15].value = this.getValidate(this.getValue('非钻杆钻铤内循环压耗'));// 其他钻具内循环压耗

        outputs[16].value = this.getValidate(v16); // 钻杆环空压耗(含接箍)
        outputs[17].value = this.getValidate(v17); // 加重钻杆环空压耗(含接箍)

        outputs[18].value = this.getValidate(v18); // 钻具内循环压耗
        outputs[19].value = this.getValidate(v19); // 钻具环空压耗

        // v3：总循环压耗
        // let v10 = this.getValidate(+this.getValidate(v0) + +this.getValidate(v1) + +this.getValidate(v2) + +this.getValidate(v3) + +this.getValidate(v5) + +this.getValidate(v6) + +this.getValidate(v7) + +this.getValidate(v8) + +this.getValidate(v9));
        let v14 = getRound(v0) + getRound(v1) + getRound(v2) + getRound(v3) + getRound(v5) + getRound(v6) + getRound(v7) + getRound(v8) + getRound(v9)+ getRound(v10)+ getRound(v11)+ getRound(v12)+ getRound(v13)+getRound(this.getValue('非钻杆钻铤内循环压耗'));        // outputs[14].value = this.getValidate(v14);
        // let a =getRound(v0) + getRound(v1)

        // let v14 = window.localStorage.getItem('总循环压耗');
        outputs[13].value = this.getValidate(v14);


        outputs.forEach(item => {
            window.localStorage.setItem(item.label, item.value);
        });


        this.setState({
            output: outputs
        });
        this.props.setBack(outputs);


        // 给android 传参
        function GetQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
        const inputData = Object.assign(this.state.input, []);
        const outData = Object.assign(this.state.output, []);
        const url = GetQueryString('tab');
        const num=outData[outData.length-1].value;
        window.AndroidFunction.save(encodeURI(JSON.stringify(window.localStorage)), "循环压耗", num, url);
    }

    setControl(specialControl, value) {
        if (specialControl === 'm') {
            this.setState({
                P_value: this.getValidate(value / 9.5 * 0.5)
            })
        } else if (specialControl === 'n') {
            this.setState({
                Q_value: this.getValidate(value / 9.5 * 0.5)
            })
        } else if (specialControl === 'p') {
            this.setState({
                P_value: value
            })
        } else if (specialControl === 'q') {
            this.setState({
                Q_value: value
            })
        }
    }


    render() {
        return <FinalCalculate inputParams={this.state.input}
                               outputParams={this.state.output}
                               setValue={this.setValue.bind(this)}
                               title={'循环压耗'}
                               formula={this.formula}
                               setControl={this.setControl.bind(this)}
                               P_value={this.state.P_value}
                               Q_value={this.state.Q_value}
        />
    }
}

module.exports = {
    Drill,
    Pipe,
    InnerCycle,
    BodyCycle,
    OtherInner,
    OtherBody,
    Total

};
