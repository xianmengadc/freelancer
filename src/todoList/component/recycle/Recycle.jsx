import React from 'react';
import Toolbar from './../common/ToolBar.jsx'
import Mode from './RecycleCalculateMode.jsx'
import './recycle.less'
class Low extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            floorValue: 0, // 地面管汇压耗
            innerloopValue: 0, // 钻具内循环压耗
            hohlraumValue: 0, // 钻具环空压耗
            totalValue: 0, // 总循环压耗

            showConfigPanel: false,
            tabs: [
                // 1
                {
                    label: '钻杆内循环压耗', code: 'inner-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82'
                },
                // 2
                {
                    label: '钻杆环空压耗(含接箍)', code: 'body-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                },
                // 3
                {
                    label: '加重钻杆内循环压耗', code: 'heavy-inner-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82'
                },
                // 4
                {
                    label: '加重钻杆环空压耗(含接箍)', code: 'heavy-body-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                },

                // 5
                {
                    label: '钻铤内循环压耗', code: 'drill-inner-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82'
                },
                // 6
                {
                    label: '钻铤环空压耗', code: 'drill-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                },
                // 7
                {
                    label: '扶正器内循环压耗', code: 'centralizer-inner-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/钻具内径^4.82'
                },
                // 8
                {
                    label: '扶正器环空压耗', code: 'centralizer-body-cycle', value: 0,
                    formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                },
                // 9
                {
                    label: '其他钻具内循环压耗', code: 'other-inner', value: 0,
                    formula: '非钻杆钻铤内循环压耗'
                },
                // 10
                {
                    label: '其他钻具环空压耗', code: 'other-body', value: 0,
                    formula: '螺杆环空压耗+螺杆扶正器环空压耗'
                },


                // {
                //     label: '地面管汇压耗', code: 'pipe', value: 0,
                //     formula: '地面管汇摩阻系数*钻井液密度*（泵排量/100）^1.86*9.818'
                // },
                // {
                //     label: '钻头压降', code: 'drill', value: 0,
                //     formula: '泵压 - 循环压耗'
                // },
                // {
                //     label: '钻杆接箍环空压耗', code: 'body-cycle-patch', value: 0,
                //     formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                // },
                // {
                //     label: '加重钻杆接箍环空压耗', code: 'heavy-body-cycle-patch', value: 0,
                //     formula: '7628*塑性粘度^0.2*钻井液密度^0.8*泵排量^1.8*钻具长度/(井眼直径-钻具外径)^3/(井眼直径+钻具外径)^1.8'
                // },
            ]
        };
    }

    componentWillMount() {
        let tabList = Object.assign(this.state.tabs, []);

        tabList.forEach(item => {
            item.value = (+window.localStorage.getItem(item.label)).toFixed(2);
            // item.value = (+window.localStorage.getItem(item.label));
            // console.log(item)
            // if (item.label == '钻杆环空压耗(含接箍)') {
            //     let a =(parseFloat(window.localStorage.getItem('钻杆环空压耗')) + parseFloat(window.localStorage.getItem('zgjg'))).toFixed(2)
            //     item.value =  a == 'NAN'? a : "0.00";
            // }
            // if (item.label == '加重钻杆环空压耗(含接箍)') {
            //     let a =(parseFloat(window.localStorage.getItem('加重钻杆环空压耗')) + parseFloat(window.localStorage.getItem('jzzgjg'))).toFixed(2)
            //     item.value = a == 'NAN'? a : "0.00";
            // }
            // if(item.label == '其他钻具内循环压耗'){
            //     item.value = parseFloat(window.localStorage.getItem('非钻杆钻铤内循环压耗')).toFixed(2);
            // }
            // if (item.label == '其他钻具环空压耗') {
            //     item.value = (parseFloat(window.localStorage.getItem('螺杆环空压耗')) + parseFloat(window.localStorage.getItem('螺杆扶正器环空压耗'))).toFixed(2);
            // }
        })

        let floorValue = 0;
        let innerloopValue = 0;
        let hohlraumValue = 0;
        let totalValue = 0;

        // 地面管汇压耗计算
        // this.state.tabs.forEach((item, key) => {
        //     if (item.label === '地面管汇压耗') {
        //         // console.log(item.value)
        //         floorValue += +item.value
        //     }
        // });

        // 钻具内循环压耗计算
        // console.log(this.state.tabs)
        // this.state.tabs.forEach((item, key) => {
        //     // console.log(item.label)
        //     if (item.label !== '钻杆环空压耗(含接箍)' && item.label !== '加重钻杆环空压耗(含接箍)' && item.label !== '钻铤环空压耗' &&
        //         item.label !== '扶正器环空压耗' && item.label !== '其他钻具环空压耗' && item.label !== '地面管汇压耗' ) {

        //         innerloopValue += +item.value
        //     }
        // });


        // 钻具环空压耗计算
        // this.state.tabs.forEach((item, key) => {
        //     if (item.label !== '钻杆内循环压耗' && item.label !== '加重钻杆内循环压耗' && item.label !== '钻铤内循环压耗' &&
        //         item.label !== '扶正器内循环压耗' && item.label !== '其他钻具内循环压耗' && item.label !== '地面管汇压耗' ) {
        //         // console.log(item.value)
        //         hohlraumValue += +item.value
        //     }

        // });
        // 总循环压耗计算
        // console.log(this.state.tabs)
        // this.state.tabs.forEach((item, key) => {
        //         totalValue += +item.value
        // });
        // window.localStorage.setItem('总循环压耗', totalValue);

        this.setState({
            tabs: tabList,
            floorValue: window.localStorage.getItem('地面管汇压耗')?window.localStorage.getItem('地面管汇压耗'):'0.00', // 地面管汇压耗
            innerloopValue: window.localStorage.getItem('钻具内循环压耗')?window.localStorage.getItem('钻具内循环压耗'):'0.00', // 钻具内循环压耗
            hohlraumValue: window.localStorage.getItem('钻具环空压耗')?window.localStorage.getItem('钻具环空压耗'):'0.00', // 钻具环空压耗
            totalValue: window.localStorage.getItem('总循环压耗')?window.localStorage.getItem('总循环压耗'):'0.00', // 总循环压耗
        })
    }

    getCalculator(code) {
        switch (code) {
            case 'pipe':
                return <Mode.Pipe setValue={this.setValue.bind(this)} code={code} />;

            case 'inner-cycle':
                return <Mode.innerCycle setValue={this.setValue.bind(this)} code={code} />;
            case 'body-cycle':
                return <Mode.BodyCycle setValue={this.setValue.bind(this)} code={code} />;

            case 'heavy-inner-cycle':
                return <Mode.innerCycle setValue={this.setValue.bind(this)} code={code} />;
            case 'heavy-body-cycle':
                return <Mode.BodyCycle setValue={this.setValue.bind(this)} code={code} />;

            case 'drill-inner-cycle':
                return <Mode.InnerCycle setValue={this.setValue.bind(this)} code={code} />;
            case 'drill-cycle':
                return <Mode.BodyCycle setValue={this.setValue.bind(this)} code={code} />;

            case 'centralizer-inner-cycle':
                return <Mode.innerCycle setValue={this.setValue.bind(this)} code={code} />;
            case 'centralizer-body-cycle':
                return <Mode.BodyCycle setValue={this.setValue.bind(this)} code={code} />;

            case 'other-inner':
                return <Mode.OtherInner setValue={this.setValue.bind(this)} code={code} />;
            case 'other-body':
                return <Mode.OtherBody setValue={this.setValue.bind(this)} code={code} />;

            case 'drill':
                return <Mode.Drill setValue={this.setValue.bind(this)} code={code} />;

            default:
                return null;
        }
    }

    setValue(v, code) {
        const tabs = Object.assign(this.state.tabs, []);
        let tab = tabs.filter(i => { return i.code === code })[0];
        tab.value = v;
        this.setState({
            tabs: tabs
        })
    }

    showConfigPanel() {
        this.setState({
            showConfigPanel: true,
        });
    }

    close() {
        this.setState({
            showConfigPanel: false
        })
    }

    setBack(values) {
        const tabList = Object.assign(this.state.tabs, []);
        // const outputs = Object.assign(this.state.outputs, []);
        values.forEach(item => {
            tabList.forEach(tabUnit => {
                if (tabUnit.label === item.label) {
                    tabUnit.value = item.value;
                }
            });
        });

        let totalValue = window.localStorage.getItem('总循环压耗')?window.localStorage.getItem('总循环压耗'):'0.00';
        let floorValue = window.localStorage.getItem('地面管汇压耗')?window.localStorage.getItem('地面管汇压耗'):'0.00';
        let innerloopValue = window.localStorage.getItem('钻具内循环压耗')?window.localStorage.getItem('钻具内循环压耗'):'0.00';
        let hohlraumValue = window.localStorage.getItem('钻具环空压耗')?window.localStorage.getItem('钻具环空压耗'):'0.00';
        this.setState({
            tabs: tabList,
            floorValue: floorValue, // 地面管汇压耗
            innerloopValue: innerloopValue, // 钻具内循环压耗
            hohlraumValue: hohlraumValue, // 钻具环空压耗
            totalValue: totalValue, // 总循环压耗
        })
    }

    configPanel() {
        if (this.state.showConfigPanel === true) {
            return (<div className='configPanel'>
                <div className='config' ref='configPanel'>
                    <img className='close' src='./image/icon/close.png' onClick={this.close.bind(this)} />
                    <Mode.Total setBack={this.setBack.bind(this)} />
                </div>
            </div>)
        }
        return null;
    }


    openSetting(item) {
        this.setState({
            showConfigPanel: true,
        })
    }

    renderPrams() {
        let list = [];
        this.state.tabs.forEach((item, key) => {
            list.push(<div className='config-unit' key={key} onClick={this.openSetting.bind(this, item)}>
                <div className='config-title'>
                    <span className='config-label'> {item.label} </span>
                </div>
                <div className='config-control'>
                    <span className='config-input' data-code={item.code}> {item.value} </span>
                </div>
            </div>)
        });
        return list;
    }

    getStyledNum(value) {

        let valStr = value.toString();
        let list = [];
        for (const i in valStr) {
            list.push(<span className='block' key={i}> {valStr[i]} </span>)
        }
        return list;
    }

    setRecycleFlool() {
        return <div className='num-div'>
            {this.getStyledNum(this.state.floorValue)}
        </div>
    }

    setRecycleInnerloop() {
        return <div className='num-div'>
            {this.getStyledNum(this.state.innerloopValue)}
        </div>
    }
    setRecycleHohlaum() {
        return <div className='num-div'>
            {this.getStyledNum(this.state.hohlraumValue)}
        </div>
    }
    setRecycleTotal() {
        return <div className='num-div'>
            {this.getStyledNum(this.state.totalValue)}
        </div>
    }



    render() {
        return <div className="recycle-bg">
            {this.configPanel()}
            <div className="open-config" onClick={this.showConfigPanel.bind(this)}>设置</div>
            <img className="recycle-img" src={`./image/recycle/recycle.gif`} />
            <div className="param-div">
                {this.renderPrams()}
            </div>
            <div className='recycle-div'>
                <div className='title'>地面管汇压耗</div>
                {this.setRecycleFlool()}
                <div className='title'>钻具内循环压耗</div>
                {this.setRecycleInnerloop()}
                <div className='title'>钻具环空压耗</div>
                {this.setRecycleHohlaum()}
                <div className='title'>总循环压耗</div>
                {this.setRecycleTotal()}
            </div>
            {/* <Toolbar navigate={this.props.navigate && this.props.navigate.bind(this)}/> */}
        </div>
    }
}
export default Low;
