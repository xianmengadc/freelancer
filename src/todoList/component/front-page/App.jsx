import React from 'react';
import Tabs from './../common/Tabs.jsx';
import Toolbar from './../common/ToolBar.jsx'
import Analysis from '../analysis/Analysis.jsx'
import Effect from '../effect/Effect.jsx'
import Purify from '../purify/Purify.jsx'
import Low from '../low/Low.jsx'
import Heavy from '../heavy/Heavy.jsx'
import Loop from '../loop/Loop.jsx'
import Recycle from '../recycle/Recycle.jsx'
import Simulation from '../simulation/Simulation.jsx'


import '../analysis/analysis.less'
import './app.less'
class App extends React.Component {

    constructor(props) {
        super(props);
        this.tabs = [
            {label: '综合分析', value: 'analysis.png', url: 'comprehensive_analysis.html', index: 1},
            {label: '净化能力', value: 'purify.png', url: 'purify.html', index: 2},
            // {label: '钻头降压', value: 'recycle.png', index: 3},
            {label: '循环压耗', value: 'recycle.png', index: 3},
            {label: '环空返速', value: 'back-speed.png', index: 4},
            {label: '钻头水功率', value: 'effect.png', url: 'effect.html', index: 5},
            {label: '起钻重浆', value: 'heavy.png', index: 6},
            {label: '堵漏替量', value: 'lou.png', index: 7},
            {label: '堵漏效果模拟', value: 'simulation.png', index: 8},
            {label: '智能分析', value: 'ai-analysis.png', index: 9}
        ];
        //let tab = +window.location.search.split('=')[1] || 0;
        let tab = +window.location.search.split('&')[0].split('=')[1] || 0;
        this.state = {
            curIndex: tab
        }
        function GetQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        }
        if(GetQueryString('id')){
            const jsonvalid = GetQueryString('id');
            const jsonval= JSON.parse(window.AndroidFunction.findById(jsonvalid));
            for(var p in jsonval){
                window.localStorage.setItem(p, jsonval[p]);
            }
        }

    }
    toPage(item) {
       /* this.setState({
            curIndex: item.index
        })*/
        window.location.href = `${window.location.protocol}//${window.location.host}${window.location.pathname}?tab=${item.index}`;
    }

    navigate(item) {
        window.location.href = `${window.location.protocol}//${window.location.host}${window.location.pathname}?tab=${item.index}`;
    }

    getFrontPage() {
        if(this.state.curIndex === 0) {
            return <div className="front-page-content">
                {/* <div className="title">
                 <span className="text">长庆钻井总公司</span>
                 </div>*/}
                <div>
                    <img className='front-page-header' src='./image/homepageImage/header.png'/>
                </div>
                <div className="tab-content">
                    <Tabs dataSource={this.tabs} pageJump={this.toPage.bind(this)} hasPageUrl={true}/>
                </div>
                {/* <Toolbar navigate={this.navigate.bind(this)}/> */}
            </div>
        }
        return null;
    }

    // 综合分析
    getAnalysis() {
        if(this.state.curIndex === 1) {
            return <Analysis  navigate={this.navigate.bind(this)}/>
        }
       return null;
    }

    // 净化能力
    getPurify() {
        if(this.state.curIndex === 2) {
            return <Purify navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 钻头水功率
    getEffect() {
        if(this.state.curIndex === 5) {
            return <Effect navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 起钻重浆
    heavy() {
        if(this.state.curIndex === 6) {
            return <Heavy navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 堵漏效果模拟
    simulation() {
        if(this.state.curIndex === 8) {
            return <Simulation navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 堵漏替量
    low() {
        if(this.state.curIndex === 7) {
            return <Low navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 环空返速
    loop() {
        if(this.state.curIndex === 4) {
            return <Loop navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    // 循环压耗
    recycle() {
        if(this.state.curIndex === 3) {
            return <Recycle navigate={this.navigate.bind(this)} />
        }
        return null;
    }

    getDefault() {
        if([9].indexOf(this.state.curIndex ) >= 0) {
            return <div className="front-page-content">
                {/* <div className="title">
                 <span className="text">长庆钻井总公司</span>
                 </div>*/}
                <div>
                    <img className='front-page-header' src='./image/homepageImage/header.png'/>
                </div>
                <div className="tab-content">
                    <Tabs dataSource={this.tabs} pageJump={this.toPage.bind(this)} hasPageUrl={true}/>
                </div>
                <Toolbar navigate={this.navigate.bind(this)}/>
            </div>
        }
    }

    render() {
        return (<div>
            {this.getFrontPage()}
            {this.getAnalysis()}
            {this.getPurify()}
            {this.getEffect()}
            {this.low()}
            {this.loop()}
            {this.recycle()}
            {this.heavy()}
            {this.simulation()}
            {this.getDefault()}
        </div>)
    }
}

export default App;
