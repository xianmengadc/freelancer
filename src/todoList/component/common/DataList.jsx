import React from 'react';
import '../common/common.less';
class DataList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showClear: false
        };
    }

    setClear() {
        this.refs[this.props.code].value = '';
        this.setState({
            showClear: false
        });
    }

    hasNumber(e) {
        this.setState({
            showClear: e.target.value !== ''
        });
        this.props.onChange && this.props.onChange(this.props.code, e.target.value);
    }


    // setInput() {

    //     if(this.props.code === 'p' || this.props.code === 'q') {
    //         return
    //         <input className='input-value' onChange={this.hasNumber.bind(this)} type="number" placeholder={`请输入${this.props.name}`} ref={this.props.code} value={this.props.defaultValue || ''}/>
    //     }
    //     return
    //     <input className='input-value' onChange={this.hasNumber.bind(this)} type="number" placeholder={`请输入${this.props.name}`} ref={this.props.code} defaultValue={this.props.defaultValue || ''}/>

    // }

    render() {
        //const randomid=Math.random().toString(16).substring(2);
        return <div className='input-field-option'>
            <label className='input-label'> {this.props.name}{this.props.unit}： </label>
            <div className='input-div'>
            {console.log(this.props.code=== 'a')}
                {this.props.code === 'a' ?
                    (
                        <div>
                            {/*<input list="woods" className='input-value' onChange={this.hasNumber.bind(this)} type="number" placeholder={`请输入${this.props.name}`} ref={this.props.code} defaultValue={this.props.defaultValue || ''} />*/}
                            {/*<datalist id="woods">*/}
                                {/*<option value="90"/>*/}
                                {/*<option value="100"/>*/}
                                {/*<option value="110"/>*/}
                                {/*<option value="120"/>*/}
                                {/*<option value="130" />*/}
                                {/*<option value="140"/>*/}
                                {/*<option value="150"/>*/}
                                {/*<option value="160"/>*/}
                                {/*<option value="170"/>*/}
                                {/*<option value="180"/>*/}
                                {/*<option value="190"/>*/}
                            {/*</datalist>*/}
                            <select onChange={this.hasNumber.bind(this)} placeholder={`请输入${this.props.name}`} defaultValue={this.props.defaultValue || ''} className='input-value'  ref={this.props.code}>
                                <option value="90">90</option>
                                <option value="100">100</option>
                                <option value="110">110</option>
                                <option value="120">120</option>
                                <option value="130">130</option>
                                <option value="140">140</option>
                                <option value="150">150</option>
                                <option value="160">160</option>
                                <option value="170">170</option>
                                <option value="180">180</option>
                                <option value="190">190</option>
                            </select>
                        </div>

                    ) : (
                        <div>
                            {/*<input list="cars" className='input-value' type="text" onChange={this.hasNumber.bind(this)}  placeholder={`请输入${this.props.name}`} ref={this.props.code} defaultValue={this.props.defaultValue || ''} />*/}
                            {/*<datalist id="cars">*/}
                                {/*<option value="216"/>*/}
                                {/*<option value="220"/>*/}
                                {/*<option value="235"/>*/}
                                {/*<option value="254"/>*/}
                                {/*<option value="304.8"/>*/}
                                {/*<option value="320"/>*/}
                                {/*<option value="400"/>*/}
                            {/*</datalist>*/}
                            <select onChange={this.hasNumber.bind(this)} placeholder={`请输入${this.props.name}`} defaultValue={this.props.defaultValue || ''} className='input-value'  ref={this.props.code}>
                                <option value="216">216</option>
                                <option value="220">220</option>
                                <option value="235">235</option>
                                <option value="254">254</option>
                                <option value="304.8">304.8</option>
                                <option value="320">320</option>
                                <option value="400">400</option>
                            </select>
                        </div>
                    )

                }
            </div>
        </div>;
    }
}

export default DataList;
