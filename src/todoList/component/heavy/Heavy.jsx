import React from 'react';
import Toolbar from './../common/ToolBar.jsx'
import Mode from './HeavyCalculateMode.jsx'
import './heavy.less'
class Heavy extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            percent: 0,
            showConfigPanel: false,
            cur: 1,
            tabs: [
                {label: '环空压耗', code: 'loop', value: 0},
                {label: '钻井液密度', code: 'liquid-density', value: 0},
                {label: '重浆密度', code: 'heavy-pulp-density', value: 0,
                    formula: '环空压耗/（9.8*垂深高度）+钻井液密度'
                },
                {label: '垂深高度', code: 'depth', value:0,
                    formula: '(环空压耗/(9.8*(重浆密度 - 钻井液密度)))*1000'
                }
            ]
        };
    }

    componentWillMount() {
        let tabList = Object.assign(this.state.tabs, []);
        tabList.forEach(item => {
            item.value = (+window.localStorage.getItem(item.label)).toFixed(2);
        })
        this.setState({
            tabs: tabList,

        })
    }

    setValue(code, v) {
        const tabs = Object.assign(this.state.tabs, []);
        let tab = tabs.filter(i => {return i.code === code})[0];
        tab.value = v;
        this.setState({
            tabs: tabs
        })
    }


    showConfigPanel() {
        this.setState({
            showConfigPanel: true
        });
    }



    setBack(values) {
        const tabList = Object.assign(this.state.tabs, []);
        values.forEach(item => {
            tabList.forEach(tabUnit => {
                if(tabUnit.label === item.label) {
                    tabUnit.value = (+item.value).toFixed(2);
                }
            });
        });
        this.setState({
            tabs: tabList,
        })
    }

    configPanel() {
        if(this.state.showConfigPanel === true) {
            return (<div className='configPanel'>
                <div className='config' ref='configPanel'>
                    <img className='close' src='./image/icon/close.png' onClick={this.close.bind(this)}/>
                    <Mode.Total setBack={this.setBack.bind(this)}/>
                </div>
            </div>)
        }
        return null;
    }

    close() {
        this.setState({
            showConfigPanel: false
        })
    }

    renderPrams() {
        let list = [];
        this.state.tabs.forEach((item, key) => {
            list.push(<div className='config-unit' key={key}>
                <div className='config-title'>
                    <span className='config-label'> {item.label} </span>
                </div>
                <div className='config-control'>
                    <span className='config-input' data-code={item.code}> {item.value} </span>
                </div>
            </div>)
        });
        return list;
    }

    render() {
        return <div className="heavy-bg">
            {this.configPanel()}
            <div className="open-config" onClick={this.showConfigPanel.bind(this)}>设置</div>
            <img className="heavy-img" src={`./image/heavy/animate-${this.state.cur}.png`}/>
            <div className="param-div">
                {this.renderPrams()}
            </div>
            {/* <Toolbar navigate={this.props.navigate && this.props.navigate.bind(this)}/> */}
        </div>
    }

    componentDidMount() {
        let i = 2;
        this.timer =  setInterval(() => {
            if(i === 8) {
                i = 1;
            }
            this.setState({
                cur: i
            });
            i++
        }, 500)
    }

    componentWillUnmount() {
        clearTimeout(this.timer)
    }
}
export default Heavy;